# README #

This repo is for storing the Data Structure & Algorithms exercises that I do.

### What is this repository for? ###

* Keeping a version of every exercise
* Having a common library already set up

### How do I navigate this mess? ###

| Folders | Description                    |
| ------------- | ------------------------------ |
| src      | General folder that contains the whole source code files   |
|src/Header Files | All the given ADT and Custom Data types are here |
|src/Source Files | Contains the exercises themselves|

### How do I use this? ###

In order for Visual Studio to get the files you have to include the directory `C:/YourPath/ProjectName/src` so everything can work.
I have only tested this so far on visual Studio, but it should be able to work on any IDE/Compiler.

### Can I fork this? ###

If you are reading this, yes, it means I have done this repo public, I highly doubt you will find this very usefull, as the best way to learn is to implement the Data Structures yourself, but if you wish, by all means.