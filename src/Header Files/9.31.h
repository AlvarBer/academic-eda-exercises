#ifndef __9_31_H
#define __9_31_H

#include "Header Files\Arbin.h"

template <class T>
class TreesWithHeap: public Arbin<T> {
public:
	bool isHeap() {
		return isHeapAux() && isSemiComplete(); //This is how we save checking SemiComplete all the time
	}

private:
	bool isHeapAux() {
		T currentElem = _ra->_elem;
		if (_ra->_iz == NULL && _ra->_dr == NULL) //Base case
			return true;
		else
			return (_ra->_iz->_elem < currentElem && _ra->_dr->_elem < currentElem && 
			TreesWithHeap(hijoIz()).isHeap() && TreesWithHeap(hijoDr()).isHeap());
	}

	bool isSemiComplete() {
		bool lastLevel;
		if (TreesWithHeap(hijoIz()).isSemiComplete() && TreesWithHeap(hijoDr()).isSemiComplete()) {
			if (!lastLevel)
				return (_ra->_iz != NULL && _ra->_dr != NULL);
			else

		}

		if (hijoIz() == NULL && hijoDr() != NULL) {
			return false;
		}

		return true;
	}
};

#endif