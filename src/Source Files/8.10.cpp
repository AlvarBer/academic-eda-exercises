#include <iostream>
using namespace std; 
#include "Header Files\Stack.h"

Stack<int> processInput();
bool canBeArranged(Stack<int> incomingTrain, Stack<int> desiredOutcome);

/*int main() {

	return 0;
}*/

bool canBeArranged(Stack<int> incomingTrain, Stack<int> desiredOutcome) {
	bool canBe;
	Stack<int> middleTrain;
	Stack<int> finalTrain;

	while (!incomingTrain.empty()) {
		if (incomingTrain.top() != desiredOutcome.top()) {
			middleTrain.push(incomingTrain.top());
			incomingTrain.pop();
		}
		else {
			finalTrain.push(incomingTrain.top());
			incomingTrain.pop();
			desiredOutcome.pop();
			while (middleTrain.top() == desiredOutcome.top()) {
				finalTrain.push(middleTrain.top());
				middleTrain.pop();
				desiredOutcome.pop();
			}
		}
	}
	middleTrain.empty() ? canBe = false : canBe = true;
	
	return canBe;
}