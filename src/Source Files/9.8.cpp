#include "Header Files\Arbin.h"

bool degenerado(Arbin<int>a) {
	if (a.esVacio()) {
		return true;
	}
	else if (a.hijoDr().esVacio() && !a.hijoIz().esVacio())
		return degenerado(a.hijoIz());
	else if (!a.hijoDr().esVacio() && a.hijoIz().esVacio())
		return degenerado(a.hijoDr());
	else
		return false;
}