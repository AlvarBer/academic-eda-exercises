#include <iostream>
using namespace std;
#include "Header Files\ImprovedList.h"

List<char> removeSpaces (List<char> &phrase);
bool eightpointtwentynine (List<char> phrase);


/*int main() {
	List<char> input;

	input = List<char>();
	input.push_back('D');
	input.push_back('a');
	input.push_back('b');
	input.push_back('a');
	input.push_back('l');
	input.push_back('e');
	input.push_back(' ');
	input.push_back('a');
	input.push_back('r');
	input.push_back('r');
	input.push_back('o');
	input.push_back('z');
	input.push_back(' ');
	input.push_back('a');
	input.push_back(' ');
	input.push_back('l');
	input.push_back('a');
	input.push_back(' ');
	input.push_back('z');
	input.push_back('o');
	input.push_back('r');
	input.push_back('r');
	input.push_back('a');
	input.push_back(' ');
	input.push_back('e');
	input.push_back('l');
	input.push_back(' ');
	input.push_back('a');
	input.push_back('b');
	input.push_back('a');
	input.push_back('d');

	input = removeSpaces(input);

	if (eightpointtwentynine(input)) {
		cout << "The phrase is a palindrome" << endl;
	}
	else {
		cout << "Sorry, not a a palindrome" << endl;
	}

	return 0;
}*/

bool eightpointtwentynine (List<char> phrase) {
	unsigned int i = 0;
	bool isPalyndrom = true;
	List<char>::ReverseIterator endIterator = phrase.reverseBegin();
	List<char>::Iterator begIterator = phrase.begin();

	endIterator.next(); //We have to do this in order to not check a null element

	while (isPalyndrom && i <= phrase.size()) {
		if (endIterator.elem() != begIterator.elem()) {
			isPalyndrom = false;
		}
		endIterator.next();
		begIterator.next();
	}

	return isPalyndrom;
}

List<char> removeSpaces (List<char> &phrase) {
	List<char>::Iterator myIterator = phrase.begin();
	for (unsigned int i = 0; i < phrase.size(); ++i) {
		cout << "Char " << i << " :" << myIterator.elem() << endl;
		if (myIterator.elem() == char(32)) {
			myIterator = phrase.erase(myIterator);
		}
		myIterator.next();
	}
	return phrase;
}