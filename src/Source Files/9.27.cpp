#include "Header Files\Arbin.h"
#include "Header Files\List.h"

template <class T>
Arbin<T> eightPointTwentyThree(List<T> preorder, List<T> inorder) {
	Arbin<T> tree;
	int i = 0;

	if (preorder.size() == 0) {
		tree = Arbin<T>();
	}
	if (preorder.size() == 1) {
		tree = Arbin<T>(preorder.front());
	}
	else {
		List<T> leftInorder = List<T>();
		List<T> leftPreorder = List<T>();
		List<T> rightInorder = List<T>();
		List<T> rightPreorder = List<T>();

		while (inorder.at(i) != preorder.at(0)) {
			leftInorder.push_back(inorder.at(i));
			++i;
		}
		for (i = 0; i < leftInorder.size(); ++i) {
			leftPreorder.push_back(preorder.at(i + 1));
		}
		for (i = leftInorder.size() + 1; i < inorder.size(); ++i) {
			rightInorder.push_back(inorder.at(i));
		}
		for (i = leftPreorder.size() + 1; i < preorder.size(); ++i) {
			rightPreorder.push_back(preorder.at(i));
		}
		tree = Arbin<T>(eightPointTwentyThree(leftPreorder, leftInorder), preorder.at(0), eightPointTwentyThree(rightPreorder, rightInorder));
	}

	return tree;
}

/*int main() {
	List<int> l1 = List<int>();
	l1.push_front(1);
	List<int> l2 = List<int>();
	l2.push_front(2);
	
	Arbin<int> tree = Arbin<int>();

	tree = eightPointTwentyThree(l1, l2);

	List<int> myl = List<int>(tree.preorden());

	return 0;
}*/