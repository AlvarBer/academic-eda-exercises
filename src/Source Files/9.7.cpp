#include "Header Files\Arbin.h"

template <class T>
bool isHomogeneus(Arbin<T> a) {
	if (a.esVacio())
		return true;
	else if (a.hijoIz().esVacio() && a.hijoDr().esVacio()) {
		return true;
	}
	else if (a.hijoIz() || a.hijoDr()) {
		return false;
	}
	else
		return (isHomogeneus(a.hijoIz()) && isHomogeneus(a.hijoDr()));
}